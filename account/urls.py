from . import views
from django.urls import path
from .views import PostList, PostCreate, PostUpdate, DeletePost, Profile
from django.contrib.auth import views as authviews

app_name = 'account'
urlpatterns = [
    path('login/', views.Login.as_view(), name='login'),
    path('logout/', authviews.LogoutView.as_view(), name='logout')
]

urlpatterns += [
    path('', PostList.as_view(), name='dashboard'),
    path('newpost/', PostCreate.as_view(), name='create'),
    path('edit/<int:pk>/', PostUpdate.as_view(), name='editpost'),
    path('delete/<int:pk>/', DeletePost.as_view(), name='delpost'),
    path('profile/', Profile.as_view(), name='profile'),
]
