from django.http import Http404
from django.shortcuts import get_object_or_404, redirect
from blog.models import Post
from django.http import Http404

class AuthorsAccessMixin():
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            if request.user.is_superuser or request.user.is_author:
                return super().dispatch(request, *args, **kwargs)
            else:
                return redirect("account:login")
        else:
            return redirect("account:login")

class AuthorAccessMixin():
	def dispatch(self, request, pk, *args, **kwargs):
		post = get_object_or_404(Post, pk=pk)
		if post.author == request.user and post.status in ['b', 'd'] or\
		request.user.is_superuser:
			return super().dispatch(request, *args, **kwargs)
		else:
			raise Http404("You can't see this page.")

class FieldsMixin():
    def dispatch(self, request, *args, **kwargs):
        self.fields = [
            "title","slug","category","image",
            "description","publish","is_special","status"
        ]
        if request.user.is_superuser:
            self.fields.append("author")
        return super().dispatch(request, *args, **kwargs)

class FormValidMixin():
    def form_valid(self, form):
        if self.request.user.is_superuser:
            form.save()
        else:
            self.obj = form.save(commit=False)
            self.obj.author = self.request.user
            if not self.obj.status == 'i':
                self.obj.status = 'd'
        return super().form_valid(form)

class SuperUserAccessMixin():
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return super().dispatch(request, *args, **kwargs)
        else:
            return Http404("you can't see this page.")