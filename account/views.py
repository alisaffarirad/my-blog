from django.shortcuts import render
from django.contrib.auth.views import LoginView
from django.urls import reverse_lazy
from .mixins import AuthorsAccessMixin, FieldsMixin, FormValidMixin, AuthorAccessMixin, SuperUserAccessMixin
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from blog.models import Post
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import User
from .forms import ProfileForm

class Login(LoginView):
    template_name = 'account/login.html'
    def get_success_url(self):
        return reverse_lazy('blog:home')
# Create your views here.

class PostList(AuthorsAccessMixin, ListView):
    template_name = 'account/dashboard.html'
    context_object_name = 'posts'

    def get_queryset(self):
        if self.request.user.is_superuser:
            return Post.objects.all()
        else:
            return Post.objects.filter(author= self.request.user)

class PostCreate(AuthorsAccessMixin, FormValidMixin, FieldsMixin, CreateView):
    model= Post
    template_name = 'account/cu_post.html'

class PostUpdate(AuthorAccessMixin, FieldsMixin, FormValidMixin, UpdateView):
    model = Post
    template_name = 'account/cu_post.html'

class DeletePost(SuperUserAccessMixin, DeleteView):
    model = Post
    success_url = reverse_lazy('account:dashboard')
    template_name = 'account/post_confirm_delete.html'

class Profile(LoginRequiredMixin ,UpdateView):
	model = User
	template_name = "account/profile.html"
	form_class = ProfileForm
	success_url = reverse_lazy("account:dashboard")

	def get_object(self):
		return User.objects.get(pk = self.request.user.pk)

	def get_form_kwargs(self):
		kwargs = super(Profile, self).get_form_kwargs()
		kwargs.update({
			'user': self.request.user
		})
		return kwargs
