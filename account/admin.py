from django.contrib import admin
from .models import User
from django.contrib.auth.admin import UserAdmin

# Register your models here.
UserAdmin.fieldsets += (
    ("user status", {'fields': ('is_author', 'special_user')}),
)

UserAdmin.fieldsets += (
    ("bio", {'fields': ('bio',)}),
)
UserAdmin.list_display += ('is_author', 'special_user')

admin.site.register(User, UserAdmin)