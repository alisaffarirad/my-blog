from django.db import models
from django.utils import timezone
from django.utils.html import format_html
from account.models import User
from django.urls import reverse

# managers
class PostManager(models.Manager):
    def published(self):
        return self.filter(status='p')

class CategoryManager(models.Manager):
    def active(self):
        return self.filter(status=True)
# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField(max_length=300, unique=True, verbose_name='category slug')
    status = models.BooleanField(default=True, verbose_name='to be displayed')
    position = models.IntegerField()
    class Meta:
        verbose_name='categroy'
        ordering = ['position']

    def __str__(self):
        return self.name

    objects = CategoryManager()

class Post(models.Model):
    STATUS_CHOICES = (
        ('d', 'draft'),
        ('p', 'published'),
        ('i', 'investigation'),
        ('b', 'back')
    )
    title = models.CharField(max_length=300, verbose_name='post title')
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='post_user')
    slug = models.SlugField(max_length=400, unique=True, verbose_name='post slug')
    category = models.ManyToManyField(Category, related_name='posts')
    image = models.ImageField(upload_to='images', verbose_name='post image')
    description = models.TextField()
    publish = models.DateTimeField(default=timezone.now, verbose_name='publication date')
    is_special = models.BooleanField(default=False, verbose_name='Special content')
    status = models.CharField(max_length=1, choices=STATUS_CHOICES)

    class Meta:
        verbose_name='Post'
        ordering = ['-publish']

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('account:dashboard')

    def category_to_list(self):
        return ", ".join([category.name for category in self.category.filter(status=True)])
    
    def image_tag(self):
        return format_html("<img width=100 height=75  src='{}'>".format(self.image.url))
        
    objects = PostManager()