from django.contrib import admin
from .models import Post, Category

# Register your models here.

class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'author','slug','category_to_list','image_tag','description','publish','is_special','status')
    list_filter = ('publish', 'status', 'author')
    search_fields = ('title', 'description')
    prepopulated_fields = {'slug':('title',)}
    ordering = ['-publish']


admin.site.register(Post, PostAdmin)

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'position','slug','status')
    list_filter = (['status'])
    search_fields= ('name', 'slug')
    prepopulated_fields = {'slug':('name',)}

admin.site.register(Category, CategoryAdmin)