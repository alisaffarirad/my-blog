from django.shortcuts import render
from django.views.generic import ListView, DetailView
from .models import Post, Category
from account.models import User
from django.shortcuts import get_object_or_404
from account.mixins import AuthorAccessMixin

class Home(ListView):
    queryset = Post.objects.published()
    paginate_by = 4
    context_object_name = 'posts'
    template_name = 'blog/home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category'] = Category.objects.active()
        return context

class PostDetail(DetailView):
    context_object_name = 'post'
    template_name= 'blog/post.html'

    def get_object(self):
        slug = self.kwargs.get('slug')
        return get_object_or_404(Post.objects.published(), slug=slug)

class CategoryList(ListView):
    paginate_by = 4
    template_name = 'blog/category_list.html'
    context_object_name = 'posts'

    def get_queryset(self):
        global category
        slug = self.kwargs.get('slug')
        category = get_object_or_404(Category.objects.active(), slug=slug)
        return category.posts.published()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category'] = category
        return context

class UserPost(ListView):
    paginate_by = 4
    template_name = 'blog/user_posts.html' 
    context_object_name = 'posts'
    def get_queryset(self):
        global author
        username = self.kwargs.get('username')
        author = get_object_or_404(User, username=username)
        return author.post_user.published()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['author'] = author
        return context

class Preview(AuthorAccessMixin, DetailView):
    template_name = 'blog/post.html'
    def get_object(self):
        pk = self.kwargs.get('pk')
        return get_object_or_404(Post, pk=pk)