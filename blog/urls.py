from django.urls import path
from .views import Home, PostDetail, CategoryList, UserPost, Preview

app_name = 'blog'
urlpatterns = [
    path('', Home.as_view() , name='home'),
    path('page=<int:page>/', Home.as_view() , name='home'),
    path('post/<slug:slug>/', PostDetail.as_view() , name='postdetail'),
    path('category/<slug:slug>/', CategoryList.as_view() , name='category'),
    path('category/<slug:slug>/page=<int:page>/', CategoryList.as_view() , name='category'),
    path('user/<slug:username>/', UserPost.as_view(), name='postsUser'),
    path('user/<slug:username>/page=<int:page>/', UserPost.as_view(), name='postsUser'),
    path('preview/<int:pk>/', Preview.as_view(), name='preview'),
]
